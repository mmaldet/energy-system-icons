# This command converts all SVG files in the `svg` directory to PNG and saves them in the
# `png` directory.
# NOTE that this requires inkscape installed
for file in readdir(joinpath(@__DIR__, "svg"))
    name, _ = splitext(file)
    input = joinpath(@__DIR__, "svg", file)
    output = joinpath(@__DIR__, "png", string(name, ".png"))
    run(`inkscape $input --export-filename=$output`)
end
